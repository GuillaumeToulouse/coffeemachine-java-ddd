package com.codurance;


import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static java.util.Arrays.asList;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

@RunWith(JUnitParamsRunner.class)
public class MyClassShould {

    @Before
    public void initialise() {

    }


    @Test
    @Parameters({
            "actual, expected"
    })
    public void beTestable(String actual, String expected) {
        Assertions.assertThat(actual).isEqualTo(expected);
     //   assertThat((actual), is(expected));


    }
}
