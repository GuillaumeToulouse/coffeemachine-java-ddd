Coffee Machine Kata
===============
http://simcap.github.io/coffeemachine/cm-first-iteration.html
# First iteration - Making drinks

In this iteration, your task is to implement the logic (starting from a simple class) that translates orders from customers of the coffee machine to the drink maker. Your code will use the drink maker protocol (see below) to send commands to the drink maker.

The coffee machine can serves 3 type of drinks: tea, coffee, chocolate.

# Use cases
Your product owner has delivered the stories and here they are:

The drink maker should receive the correct instructions for my coffee / tea / chocolate order
I want to be able to send instructions to the drink maker to add one or two sugars
When my order contains sugar the drink maker should add a stick (touillette) with it

# Drink maker protocol
The drink maker receives string commands from your code to make the drinks. It can also deliver info messages to the customer if ordered so. The instructions it receives follow this format:

`"T:1:0" (Drink maker makes 1 tea with 1 sugar and a stick)`

`"H::" (Drink maker makes 1 chocolate with no sugar -
and therefore no stick)`

`"C:2:0" (Drink maker makes 1 coffee with 2 sugars and a stick)`

`"M:message-content" (Drink maker forwards any message received
onto the coffee machine interface
for the customer to see)`

# 1st Implementation details
You can represent the incoming order of the customer as you wish. For instance, it could be a simple POJO that contains the order details, or a simple String, try to think of the simplest thing that do the job. Complex matters will arrive soon enough, trust us.

# Next step
[Finished... tell your PO you're going forward](http://simcap.github.io/coffeemachine/cm-second-iteration.html)

# DDD tactique
Lisez https://blog.engineering.publicissapient.fr/2018/06/25/craft-les-patterns-tactiques-du-ddd/

## After the 2nd step, it's time to apply technical patterns
je vous explique en franglais:
1) [Make illegal states unrepresentable](https://khalilstemmler.com/articles/typescript-domain-driven-design/make-illegal-states-unrepresentable/):  les nulls ne sont pas autorisés.
   Utilisez Optionnal de façon appropriée, faire sauter 1 if (avec map) -> vous avez diminué la complexité cyclomatique.
1) Chassez les Primitives (primitive obession):
   les entiers, bool, et autre strings doivent devenir des value objects et porter leur propre règle métier.
   Faites cela pour la valeur 'Sucre'. Faites en un Value Object -> vous avez diminué la complexité cyclomatique.
2) La fonction getString est encore trop complexe. Extérioriser le controle du flouze dans une autre fonction.
   Vous devez pourtant gérer une rupture du flot d'exécution de l'appelant (si il n'y a pas assez d'argent pour faire une boisson).
   Pensez qu'en fait vous devez faire face à un jeu à 2 scénarios (ou bien/ou bien)
   Traduisez en anglais et cherchez sur google: [Either](https://www.baeldung.com/vavr-either).
2) Refactor to immutabilty : la valeur flouze est mutable. Cela rend difficile que votre code soit threadSafe.
   En même temps, le principe SRP vous dit que chaque classe doit avoir une seule responsabilité.
   Faite gérer le flouze par une autre entité.

3)  Protégez les entités de votre domaine: personne ne doit être autorisé à faire des 'new' à part la racine d'aggregat.
    Utilisez des factories (voir article public sapiens)

4) Onion/Hexa Architecture: le domaine doit être pur POJO et aucun effet de bord n'est admis
   c'est à dire aucune dépendance, aucun usage de java.io.* (à part dans les tests)
   pour cela vous devez séparer la partie qui produit la sortie de la string du protocole machine à boisson
   ("H:0:") des objets du domaine. Vous allez créer un output adapter. Vous allez aussi devoir créer un Driving Adapter.
   Lisez [ceci pour plus d'explication](https://blog.octo.com/architecture-hexagonale-trois-principes-et-un-exemple-dimplementation/).
   Utilisez la racine d'agrégat comme porte d'entrée des 'commands' dans votre core domain.

5) Event Driven:
   le Core Domain ne peut communiquer vers l'extérieur que par [évennements](https://docs.microsoft.com/fr-fr/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/domain-events-design-implementation).
   Mettez en oeuvre un bus de service ultra simple (en POJO, aucun import autorisé)
   Et écrivez un adapteur simple, qui sera branché au bus de service et affichera sur la console la chaine de caractere du protocole.

6) Repository agnostic: on veut pouvoir sauver l'état de notre application dans une unité qui garantie le stockage (une SGBDR, un noSql, un S3, whatever)
   Ecrivez un nouvel adapteur qui sait persister de manière simple (sérialisation JSON). Cet adapteur doit pouvoir être interchangeable. Il doit donc implémenter une interface qui soit
   assez générique pour permettre cet échange.
   Le coeur métier ne doit pas être impacté par l'adapteur de persistence (principe de l'ACL).
   L'adapteur peut utiliser des DTO qui lui sont nécessaires (pour le mapping).
   Quand est ce que le Repository adapter est déclenché?  2 options: soit le Driver Service s'en charge (ou l'application si vous voulez);
   soit c'est à chaque fois que des évènements sont dans le bus qu'on décide de persister (quoi et comment?).


**Règle d'or: le coeur métier n'a aucun connaissance des services/adapteurs.**
   